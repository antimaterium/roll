#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define VERBOSE 		1<<0
#define SUM     		1<<1
#define DROP_LOW 		1<<2
#define DROP_HIGH		1<<3 
#define VERBOSE_SORT	1<<4

#define DEFAULT SUM

struct diceroll{
	int num;
	int type;
};

struct state{
	struct diceroll roll;
	long int mode;
	int low_drop_no;
	int high_drop_no;
	int * rolls;
	int sum;
};

int isNum(char input){
	int is_a_number = 0;
	if(48 <= input <= 57) is_a_number = 1;
	return is_a_number;
}

struct state * initState(){
	struct state * pstate = malloc(sizeof(struct state));
	pstate->mode = DEFAULT;
	pstate->low_drop_no = 0;
	pstate->high_drop_no = 0;
	pstate->sum = 0;
	return pstate;
}

struct state * parseInputs(int argc, char * argv[], struct state * pstate){
	if(argc < 2){
		printf("Error, need an input\n");
		return 0;
	}
	int i = 1;
	for(; i < argc; i++){
		char front = argv[i][0];
		if((isNum(front))&&(front != '-')){
			char * input = argv[i];
			char * number;
			number = strsep(&input, "d");
			if((number == NULL) || (!isNum(*number))) ;
			else pstate->roll.num = atoi(number);
			number = strsep(&input, "d");
			if((number == NULL) || (!isNum(*number))) ;
			else pstate->roll.type = atoi(number);
		}
		if(strlen(argv[i]) < 2) continue;
		if(front == '-'){
			char mod = argv[i][1];	
			if (mod == 'v') pstate->mode = (pstate->mode | VERBOSE);
			if (mod == 's') pstate->mode = (pstate->mode | SUM);
			if (mod == 'd') {	if(strlen(argv[i]) < 3) continue;
								int hilo = -1;
								if(argv[i][2] == 'h'){
						pstate->mode = (pstate->mode | DROP_HIGH);	
						hilo = 1;					 }
								if(argv[i][2] == 'l'){
						pstate->mode = (pstate->mode | DROP_LOW);	
						hilo = 0;					 }
								if(strlen(argv[i]) >= 4){ 
								if(!isNum(argv[i][3])) continue;
								if(hilo)
								pstate->high_drop_no = 
								(int)(argv[i][3]) - 48;
								else
								pstate->low_drop_no = 
								(int)(argv[i][3]) - 48;
								}
								else if(strlen(argv[i]) < 4){ 
								if(hilo)
								pstate->high_drop_no = 1;
								else
								pstate->low_drop_no = 1;
								}

							}
		}
	}
	if(pstate->low_drop_no > pstate->roll.num) 
		pstate->low_drop_no = pstate->roll.num;	
	if(pstate->high_drop_no > pstate->roll.num) 
		pstate->high_drop_no = pstate->roll.num;	
}

int comp(int a, int b, long int mode){
	//4 possibilities:
	//a > b and drop high: a is droppable
	//a < b and drop low:  a is droppable
	//a < b and drop high: b is droppable
	//a > b and drop low:  b is droppable
	if ((a > b)&&(mode == DROP_HIGH)) return 0;
	if ((a < b)&&(mode == DROP_LOW)) return 0;
	return 1;
};

void sort(int n, int * arr, long int mode, struct state * pstate){
int sorted = 0;
while(sorted == 0){
	sorted = 1;
	for(int i = 1; i < n; i++){
		if (!comp(arr[i - 1], arr[i], mode)){
			if(pstate->mode & VERBOSE_SORT) printf("not sorted\n");
			sorted = 0;
			break;
		}
	}
	if (sorted == 1) break;
	for(int i = 1; i < n; i++){
		if (!comp(arr[i - 1], arr[i], mode)){
			int t = arr[i - 1];
			arr[i - 1] = arr[i];
			arr[i] = t;
		}
	}
	if(pstate->mode & VERBOSE_SORT){
	for(int i = 0; i < n; i++) printf("%d ", arr[i]);
	printf("\n");
	}
}
}

void rolldice(struct state * pstate){
	pstate->rolls = malloc(pstate->roll.num*sizeof(int));
	for(int i = 0; i < pstate->roll.num; i++){
	pstate->rolls[i] = (rand() % pstate->roll.type) + 1;
	}
}

int snip(struct state * pstate, int p){
	int * newArr = malloc(p*sizeof(int));
	for(int i = 0; i < p; i++){
		newArr[i] = pstate->rolls[i];
	}
	free(pstate->rolls);
	pstate->rolls = newArr;
	pstate->roll.num = p;
	return 0;
};

int gtz(int a){if (a < 0)return 0; return a;};

void manipResults(struct state * pstate){
	if(pstate->mode & DROP_HIGH){
		sort(pstate->roll.num, pstate->rolls, DROP_HIGH, pstate);
		if(pstate->mode & VERBOSE){
			printf("Dropping %d highest rolls. Rolls dropped are:\n", pstate->high_drop_no);
			for(int i = pstate->roll.num-1; 
			i > gtz(pstate->roll.num - pstate->high_drop_no-1); i--){
				printf("%d ", pstate->rolls[i]);
			}
			printf("\n");
		}
		snip(pstate, gtz(pstate->roll.num - pstate->high_drop_no));
	}
	if(pstate->mode & DROP_LOW){
		sort(pstate->roll.num, pstate->rolls, DROP_LOW, pstate);
		if(pstate->mode & VERBOSE){
			printf("Dropping %d lowest rolls. Rolls dropped are:\n", pstate->low_drop_no);
			for(int i = pstate->roll.num-1; 
			i > gtz(pstate->roll.num - pstate->low_drop_no-1); i--){
				printf("%d ", pstate->rolls[i]);
			}
			printf("\n");
		}
		snip(pstate, gtz(pstate->roll.num - pstate->low_drop_no));
	}
	if(pstate->mode & VERBOSE){
		printf("Final rolls:\n");
		for(int i = 0; i < pstate->roll.num; i++) printf("%d ", pstate->rolls[i]);
		printf("\n");}
	if(pstate->mode & SUM){
		for(int i = 0; i < pstate->roll.num; i++){
			pstate->sum += pstate->rolls[i];
		}
		printf("%d\n", pstate->sum);
	}
		
}

int main(int argc, char * argv[]){
	srand(time(NULL)* getpid());
	struct state * pstate = initState();
	parseInputs(argc, argv, pstate);
	char * input = argv[1];

	
	if(pstate->roll.type == 0){
		printf("Can't roll a 0-sided die, you moron.\n");
		return -1;
	}
	if(pstate->roll.num == 0){
		printf("What's the point in rolling 0 dice?\n");
		return -1;
	}
	if(pstate->mode & VERBOSE){
		printf("Rolling %d %d-sided Dice...\n",
		pstate->roll.num, pstate->roll.type);
	}
	rolldice(pstate);
	manipResults(pstate);
	free(pstate->rolls);
	free(pstate);	
	return 0;
}
